import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


import { Column } from '../classes/column';
import { Utils } from '../classes/utils';
import { Task } from '../classes/task';


@Injectable()
export class BoardService {

  // rest endpoints - backend
  columnsAPI= 'https://pentascrum-b922.restdb.io/rest/columns';
  tasksAPI = 'https://pentascrum-b922.restdb.io/rest/tasks';

  private tasksSubject$: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>([]);

  private columnsSubject$: BehaviorSubject<Column[]> = new BehaviorSubject<Column[]>([]);

  private assigneeFilter$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  private httpOptions = {
      headers: new HttpHeaders({ 'x-apikey': '5a61ab987d7ef24c5cf08db7' })
   };

  constructor( private http: HttpClient) { }

  public fetchColumnList(): void {
    this.http.get<Column[]>(this.columnsAPI, this.httpOptions)
    .pipe(
      map(unorderedListOfColumns => unorderedListOfColumns.sort(Utils.columnComparator))
    )
    .subscribe((nextVal) => this.columnsSubject$.next(nextVal));
  }

  public getColumnListSubject(): Observable<Column[]> {
    return this.columnsSubject$;
  }

  public createColumn(column: Column) {
    return this.http
      .post(this.columnsAPI, column, this.httpOptions)
      .toPromise()
      .catch(this.handleError);
  }

  public fetchTasksList(): void {
    this.http.get<Task[]>(this.tasksAPI, this.httpOptions)
      .subscribe((nextVal) => this.tasksSubject$.next(nextVal));
  }

  public getTasksListSubject(): Observable<Task[]> {
    return this.tasksSubject$;
  }

  public createTask(task: Task) {
    return this.http
      .post(this.tasksAPI, task, this.httpOptions)
      .toPromise()
      .catch(this.handleError);
  }

  public updateTask(task: Task) {
    return this.http
      .put(this.tasksAPI + '/' + task._id, task, this.httpOptions)
      .toPromise()
      .catch(this.handleError);
  }

  public getTaskById(taskId: string) {
    return this.http
      .get<Task>(this.tasksAPI + '/' + taskId, this.httpOptions);
  }

  public getAssigneeFilter(): BehaviorSubject<string> {
    return this.assigneeFilter$;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
